namespace cards
{
    internal class Card
    {
        public string Suit { get; set; }
        public string Value { get; set; }

        public override string ToString() {
          return $"{this.Value} of {this.Suit}";
        }
    }
}