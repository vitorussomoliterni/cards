﻿using System;
using System.Collections.Generic;

namespace cards
{
    class Program
    {
        static void Main(string[] args)
        {
            var deck = InitialiseDeck();
            var rnd = new Random();
            var firstCard = PickRandomCard(ref deck, rnd);
            Console.WriteLine(firstCard);
            var secondCard = PickRandomCard(ref deck, rnd);
            Console.WriteLine(secondCard);
        }

        static List<Card> InitialiseDeck()
        {
            var values = new string[] {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
            var suits = new string[] {"Spades", "Hearts", "Diamonds", "Clubs"};

            var deck = new List<Card>();

            foreach (var s in suits)
            {
                foreach (var v in values)
                {
                    deck.Add(new Card {
                        Value = v,
                        Suit = s
                    });
                }
            }

            return deck;
        }

        static Card PickRandomCard(ref List<Card> deck, Random rnd)
        {
            var randomCardPosition = rnd.Next(deck.Count);
            var card = deck[randomCardPosition];
            deck.Remove(card);
            return card;
        }
    }
}
